import numpy as np
import pandas as pd
from sklearn.metrics import silhouette_score
from sklearn.metrics import calinski_harabasz_score
from sklearn.metrics import davies_bouldin_score
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from classifiers.fuzzy_c_means import FuzzyCMeans
from classifiers.k_means import KMeans
from classifiers.k_medF import KMedoidFast
from classifiers.NBOptics import Optics
from utils.distance_utils import distance_dict
import matplotlib.colors as colors
from datetime import datetime

colors = list(colors._colors_full_map.values())


class Aggregator:
	"""
		Class which will train different classifiers with different parameters
		it will print all and display/plot the best combination
	"""

	def __init__(self):
		self._results = {'k_means': {}, 'k_medoids_fast': {}, 'fuzzy_c_means': {}}
		self._results_op = {"euclidean": {}, "l1": {}, "chebyshev": {}}
		self._metrics = {'k_means': {}, 'k_medoids_fast': {}, 'fuzzy_c_means': {}}
		self._metrics_op = {"euclidean": {}, "l1": {}, "chebyshev": {}}
		self._final_metrics = {'k_means': {}, 'k_medoids_fast': {}, 'fuzzy_c_means': {}}
		self._final_metrics_op = {"euclidean": {}, "l1": {}, "chebyshev": {}}
		self._confusion_matrix = dict()
		self._confusion_matrix_op = dict()
		self.neighbor_algorithims = ["ball_tree", "kd_tree", "brute"]

		self._pca_df = None
		self._df = None

	def evaluate(self, data, n_iter):
		"""
			train different algorithms with different inputs and save the data in internal map
		"""
		self._df = data
		km = KMeans()
		kmd = KMedoidFast()
		fcm = FuzzyCMeans()
		for k in range(2, 11):
			self._metrics['k_means'][k] = {}
			self._metrics['k_medoids_fast'][k] = {}
			self._metrics['fuzzy_c_means'][k] = {}

			self._results['k_means'][k] = {}
			self._results['k_medoids_fast'][k] = {}
			self._results['fuzzy_c_means'][k] = {}

			for j in range(n_iter):
				print(datetime.now())
				print(f'No. cluster {k} and iteration {j}')
				now = datetime.now()

				current_time = now.strftime("%H:%M:%S")
				print("Current Time =", current_time)

				#
				# evaluating K-Means algorithm
				#
				_ = km.train(data, k, distance_dict['l2'], seed=j)
				labels_km = km.predict(data, distance_dict['l2'])
				sh_score_km = silhouette_score(data, labels_km)
				calinski_harabasz_score_km = calinski_harabasz_score(data, labels_km)
				davies_bouldin_score_km = davies_bouldin_score(data, labels_km)
				self._results['k_means'][k][j] = {
					'labels': labels_km,
				}

				self._metrics['k_means'][k][j] = {
					'silhouette_score': sh_score_km,
					'calinski_harabasz_score': calinski_harabasz_score_km,
					'davies_bouldin_score': davies_bouldin_score_km
				}

				#
				# 	evaluating K-Medoids algorithm
				#
				_ = kmd.train(data, k, distance_dict['l2'], seed=j)
				labels_kmd = km.predict(data, distance_dict['l2'])

				sh_score_kmd = silhouette_score(data, labels_kmd)
				calinski_harabasz_score_kmd = calinski_harabasz_score(data, labels_kmd)
				davies_bouldin_score_kmd = davies_bouldin_score(data, labels_kmd)

				self._results['k_medoids_fast'][k][j] = {
					'labels': labels_kmd,
				}

				self._metrics['k_medoids_fast'][k][j] = {
					'silhouette_score': sh_score_kmd,
					'calinski_harabasz_score': calinski_harabasz_score_kmd,
					'davies_bouldin_score': davies_bouldin_score_kmd
				}

				#
				#	evaluating Fuzzy-C-Means algorithms
				#
				_ = fcm.train(data, k, distance_dict['l2'], seed=j)
				labels_fcm = fcm.predict()

				sh_score_fcm = silhouette_score(data, labels_fcm)
				calinski_harabasz_score_fcm = calinski_harabasz_score(data, labels_fcm)
				davies_bouldin_score_fcm = davies_bouldin_score(data, labels_fcm)

				self._results['fuzzy_c_means'][k][j] = {
					'labels': labels_fcm
				}

				self._metrics['fuzzy_c_means'][k][j] = {
					'silhouette_score': sh_score_fcm,
					'calinski_harabasz_score': calinski_harabasz_score_fcm,
					'davies_bouldin_score': davies_bouldin_score_fcm,
					'optimal_k': fcm.compute_performance_index(data)
				}

	def _compute_internal_metrics_results(self):
		"""
			function which calculate stas about result of training with different value of
			on each iteration
		"""
		final_metrics = {
			'k_means': {
				'silhouette_score': {'min': [], 'max': [], 'mean': []},
				'calinski_harabasz_score': {'min': [], 'max': [], 'mean': []},
				'davies_bouldin_score': {'min': [], 'max': [], 'mean': []}
			},
			'k_medoids_fast': {
				'silhouette_score': {'min': [], 'max': [], 'mean': []},
				'calinski_harabasz_score': {'min': [], 'max': [], 'mean': []},
				'davies_bouldin_score': {'min': [], 'max': [], 'mean': []}
			},
			'fuzzy_c_means': {
				'silhouette_score': {'min': [], 'max': [], 'mean': []},
				'calinski_harabasz_score': {'min': [], 'max': [], 'mean': []},
				'davies_bouldin_score': {'min': [], 'max': [], 'mean': []}
			}
		}

		for alg, dct in self._metrics.items():
			for k, k_dct in dct.items():
				mm = {'silhouette_score': [], 'calinski_harabasz_score': [], 'davies_bouldin_score': []}

				for iteration, iterations_results in k_dct.items():
					mm['silhouette_score'].append(iterations_results['silhouette_score'])
					mm['calinski_harabasz_score'].append(iterations_results['calinski_harabasz_score'])
					mm['davies_bouldin_score'].append(iterations_results['davies_bouldin_score'])

				for metric in ['silhouette_score', 'calinski_harabasz_score', 'davies_bouldin_score']:
					tmp = np.array(mm[metric])
					final_metrics[alg][metric]['max'].append(tmp.max())
					final_metrics[alg][metric]['min'].append(tmp.min())
					final_metrics[alg][metric]['mean'].append(tmp.mean())

		self._final_metrics = final_metrics

	def plot_metrics_with_error(self):
		"""
			plot error metrics
		"""
		self._compute_internal_metrics_results()
		_colors = {
			'silhouette_score': 'b',
			'calinski_harabasz_score': 'g',
			'davies_bouldin_score': 'y'
		}

		fig, axs = plt.subplots(nrows=3, ncols=len(self._final_metrics.keys()), figsize=(30, 15))

		for ax, col in zip(axs[0, :], self._final_metrics.keys()):
			ax.set_title(col, size=14)

		for ax, row in zip(axs[:, 0], ['Silhouette', 'Calinski', 'Davies Bouldin']):
			ax.set_ylabel(row, size=14)

		for i, (alg, scores_dict) in enumerate(self._final_metrics.items()):
			for j, (score, results) in enumerate(scores_dict.items()):
				min_err = np.array(results['mean']) - np.array(results['min'])
				max_err = np.array(results['max']) - np.array(results['mean'])
				err = np.array([min_err, max_err])
				axs[j][i].errorbar(np.arange(2, 11), results['max'], yerr=err, marker='o', color=_colors[score])
		plt.show()

	def plot_scatter_for_k(self, k_dict, n_iter):
		"""
			this function shows the scatter with values desired number of cluster
			based on previous trainnig
			:param k: number of clusters

		"""
		np.random.seed(10)
		self._pca_df = self._pca_df if self._pca_df is not None else self._get_pca_data()
		fig, axs = plt.subplots(nrows=1, ncols=len(self._results), figsize=(20, 4))
		for i, current_alg in enumerate(self._results):
			k = k_dict[current_alg]
			iter_choice = np.random.choice(n_iter)
			labels = self._results[current_alg][k][iter_choice]['labels']
			plt.sca(axs[i])
			plt.title(f'{current_alg}')
			for kk in range(k):
				lbl_kk = self._pca_df[labels == kk]
				axs[i].scatter(lbl_kk[:, 0], lbl_kk[:, 1], color=colors[kk + 8])

		plt.show()

	def scatter_actual_label(self, actual_labels):
		for kk in np.unique(actual_labels):
			lbl = self._pca_df[actual_labels == kk]
			plt.scatter(lbl[:, 0], lbl[:, 1], color=colors[kk + 8])
		plt.show()

	def compute_confusion_matrix(self, k_dict, true_labels, n_iter):
		"""
		"""
		np.random.seed(10)
		for algo in list(self._results):
			iter_choice = np.random.choice(n_iter)
			pred_labels = self._results[algo][k_dict[algo]][iter_choice]['labels']
			TP, FP, TN, FN = 0, 0, 0, 0
			for i in range(len(pred_labels)):
				for j in range(i + 1, len(pred_labels)):
					y_p_i, y_p_j = pred_labels[i], pred_labels[j]
					y_t_i, y_t_j = true_labels[i], true_labels[j]
					if y_p_i == y_p_j and y_t_i == y_t_j:
						TP += 1
					elif y_p_i == y_p_j and y_t_i != y_t_j:
						FP += 1
					elif y_p_i != y_p_j and y_t_i != y_t_j:
						TN += 1
					elif y_p_i != y_p_j and y_t_i == y_t_j:
						FN += 1

			# TP / (TP + FP + FN)

			self._confusion_matrix[algo] = {
				'TP': TP,
				'FP': FP,
				'TN': TN,
				'FN': FN
			}
			print(f'Confusion matrix of algorithm: {algo}')
			print(self._confusion_matrix[algo])

	def plot_metrics_matching_sets(self, include_f1_score=True, include_precision=True, include_recall=True):

		if not include_f1_score and not include_precision and not include_recall:
			return

		df = []
		for algo in list(self._results):

			TP = self._confusion_matrix[algo]['TP']
			FP = self._confusion_matrix[algo]['FP']
			FN = self._confusion_matrix[algo]['FN']

			precision = TP / (TP + FP)
			recall = TP / (TP + FN)
			f1_score = 2 / (1 / precision + 1 / recall)

			if include_f1_score:
				df.append([algo, 'f1_score', f1_score])
			if include_precision:
				df.append([algo, 'precision', precision])
			if include_recall:
				df.append([algo, 'recall', recall])

		df = pd.DataFrame(df, columns=['Algorithms', 'metrics', 'val'])
		df = df.pivot(index='metrics', columns='Algorithms', values='val')
		df.plot(kind='bar')
		plt.title('Metrics matching sets')
		plt.show()
		print(df)

	def plot_metrics_p2p_correlation(self, include_jaccard_coef=True, include_rand_coef=True, include_folkes=True):

		if not include_jaccard_coef and not include_rand_coef and not include_folkes:
			return

		df = []

		for algo in list(self._results):
			TP = self._confusion_matrix[algo]['TP']
			FP = self._confusion_matrix[algo]['FP']
			FN = self._confusion_matrix[algo]['FN']
			TN = self._confusion_matrix[algo]['TN']
			M = TP + FP + FN + TN

			jaccard = TP / (TP + FP + FN)
			rand = (TP + TN) / M
			folkes = np.sqrt((TP / (TP + FP) * TP / (TP + FN)))

			if include_jaccard_coef:
				df.append([algo, 'jaccard', jaccard])
			if include_rand_coef:
				df.append([algo, 'rand', rand])
			if include_folkes:
				df.append([algo, 'folkes', folkes])

		df = pd.DataFrame(df, columns=['Algorithms', 'metrics', 'val'])
		df = df.pivot(index='metrics', columns='Algorithms', values='val')
		df.plot(kind='bar')
		plt.title('Metrics peer to peer correlation')
		plt.show()
		print(df)

	def _get_pca_data(self):
		"""
			apply principal component analysis to data frame from 2d ploting
		"""
		pca = PCA(n_components=2)
		return pca.fit_transform(self._df)

	################
	# OPTICS
	##############

	def evaluate_OPTICS(self, data, samp=5, x1=0.05, clust1=.05):

		self._df = data

		self._pca_df = self._pca_df if self._pca_df is not None else self._get_pca_data()

		for i in range(2, 5):
			"""
				test the various versions of the Optics distances
			"""
			distName = "euclidean"
			if i == 3:
				distName = "l1"
			if i == 4:
				distName = "chebyshev"
			for neighbor in self.neighbor_algorithims:
				print(f'No. cluster {distName} and iteration {neighbor}')
				now = datetime.now()

				current_time = now.strftime("%H:%M:%S")
				print("Current Time =", current_time)

				op = Optics(distance_metric=i, neighbor_alg=neighbor)

				self._metrics_op[distName][neighbor] = {}

				self._results_op[distName][neighbor] = {}

				#
				# evaluating algorithms
				#
				# _ = op.train(data, )
				labels_km = op.train(data, samples=samp, x=x1, clust=clust1, pcaVal=self._pca_df)
				sh_score_km = silhouette_score(data, labels_km)
				calinski_harabasz_score_km = calinski_harabasz_score(data, labels_km)
				davies_bouldin_score_km = davies_bouldin_score(data, labels_km)

				self._results_op[distName][neighbor] = {
					'labels': labels_km,
				}

				self._metrics_op[distName][neighbor] = {
					'silhouette_score': sh_score_km,
					'calinski_harabasz_score': calinski_harabasz_score_km,
					'davies_bouldin_score': davies_bouldin_score_km
				}

	def compute_confusion_matrix_OPTICS(self, dist_dict, true_labels):
		"""
		"""
		np.random.seed(10)
		for distance in list(self._results_op):

			pred_labels = self._results_op[distance][dist_dict[distance]]['labels']
			TP, FP, TN, FN = 0, 0, 0, 0
			numPos = 0
			for i, elem1 in enumerate(true_labels):
				for j in range(i + 1, len(true_labels)):
					elem2 = true_labels[j]
					if elem2 == elem1:
						numPos += 1
			numNeg = int(len(true_labels) * (len(true_labels) - 1) / 2 - numPos)

			for i in range(len(pred_labels)):
				y_p_i = pred_labels[i]
				if y_p_i != -1:
					for j in range(i + 1, len(pred_labels)):
						y_p_j = pred_labels[j]
						if y_p_j != -1:
							y_t_i, y_t_j = true_labels[i], true_labels[j]
							if y_p_i == y_p_j and y_t_i == y_t_j:
								TP += 1
							elif y_p_i == y_p_j and y_t_i != y_t_j:
								FP += 1
							elif y_p_i != y_p_j and y_t_i != y_t_j:
								TN += 1
							elif y_p_i != y_p_j and y_t_i == y_t_j:
								FN += 1

			# TP / (TP + FP + FN)

			self._confusion_matrix_op[distance] = {
				'TP': TP,
				'FP': FP,
				'TN': TN,
				'FN': FN,
				'numPos': numPos,
				'numNeg': numNeg
			}
			print(f'Confusion matrix of algorithm: {distance}')
			print(self._confusion_matrix_op[distance])

	def plot_metrics_p2p_correlation_OPTICS(self, include_jaccard_coef=True, include_rand_coef=True, include_folkes=True):

		if not include_jaccard_coef and not include_rand_coef and not include_folkes:
			return

		df = []

		for distance in list(self._results_op):
			TP = self._confusion_matrix_op[distance]['TP']
			FP = self._confusion_matrix_op[distance]['FP']
			FN = self._confusion_matrix_op[distance]['FN']
			TN = self._confusion_matrix_op[distance]['TN']
			numPos = self._confusion_matrix_op[distance]['numPos']
			numNeg = self._confusion_matrix_op[distance]['numNeg']

			M = numPos + numNeg

			jaccard = TP / (FP + numPos)
			rand = (TP + TN) / M
			folkes = np.sqrt((TP / (TP + FP) * TP / numPos))

			if include_jaccard_coef:
				df.append([distance, 'jaccard', jaccard])
			if include_rand_coef:
				df.append([distance, 'rand', rand])
			if include_folkes:
				df.append([distance, 'folkes', folkes])

		df = pd.DataFrame(df, columns=['Distance', 'metrics', 'val'])
		df = df.pivot(index='metrics', columns='Distance', values='val')
		df.plot(kind='bar')
		plt.title('Metrics peer to peer correlation')
		plt.show()

	def plot_metrics_matching_sets_OPTICS(self, include_f1_score=True, include_precision=True, include_recall=True):

		if not include_f1_score and not include_precision and not include_recall:
			return

		df = []
		for distance in list(self._results_op):

			TP = self._confusion_matrix_op[distance]['TP']
			FP = self._confusion_matrix_op[distance]['FP']
			FN = self._confusion_matrix_op[distance]['FN']
			TN = self._confusion_matrix_op[distance]['TN']
			numPos = self._confusion_matrix_op[distance]['numPos']
			numNeg = self._confusion_matrix_op[distance]['numNeg']

			M = numPos + numNeg

			precision = TP / (TP + FP)
			recall = TP / numPos
			f1_score = 2 / (1 / precision + 1 / recall)

			if include_f1_score:
				df.append([distance, 'f1_score', f1_score])
			if include_precision:
				df.append([distance, 'precision', precision])
			if include_recall:
				df.append([distance, 'recall', recall])

		df = pd.DataFrame(df, columns=['Distance', 'metrics', 'val'])
		df = df.pivot(index='metrics', columns='Distance', values='val')
		df.plot(kind='bar')
		plt.title('Metrics matching sets')
		plt.show()
		print(df)

	def _compute_internal_metrics_results_OPTICS(self):
		"""
			function which calculate stas about result of training with different value of
			on each iteration
		"""
		final_metrics = {
			'euclidean': {
				'silhouette_score': {'min': [], 'max': [], 'mean': []},
				'calinski_harabasz_score': {'min': [], 'max': [], 'mean': []},
				'davies_bouldin_score': {'min': [], 'max': [], 'mean': []}
			},
			'l1': {
				'silhouette_score': {'min': [], 'max': [], 'mean': []},
				'calinski_harabasz_score': {'min': [], 'max': [], 'mean': []},
				'davies_bouldin_score': {'min': [], 'max': [], 'mean': []}
			},
			'chebyshev': {
				'silhouette_score': {'min': [], 'max': [], 'mean': []},
				'calinski_harabasz_score': {'min': [], 'max': [], 'mean': []},
				'davies_bouldin_score': {'min': [], 'max': [], 'mean': []}
			}
		}
		for dist in self._metrics_op:
			dist_result = self._metrics_op[dist]
			for neighbor in dist_result:
				mm = {'silhouette_score': [], 'calinski_harabasz_score': [], 'davies_bouldin_score': []}
				values = dist_result[neighbor]
				mm['silhouette_score'].append(values['silhouette_score'])
				mm['calinski_harabasz_score'].append(values['calinski_harabasz_score'])
				mm['davies_bouldin_score'].append(values['davies_bouldin_score'])

				for metric in ['silhouette_score', 'calinski_harabasz_score', 'davies_bouldin_score']:
					tmp = np.array(mm[metric])
					final_metrics[dist][metric]['max'].append(tmp.max())
					final_metrics[dist][metric]['min'].append(tmp.min())
					final_metrics[dist][metric]['mean'].append(tmp.mean())
		self._final_metrics_op = final_metrics

	def plot_metrics_with_error_OPTICS(self):
		"""
			plot error metrics
		"""
		self._compute_internal_metrics_results_OPTICS()
		_colors = {
			'silhouette_score': 'b',
			'calinski_harabasz_score': 'g',
			'davies_bouldin_score': 'y'
		}

		fig, axs = plt.subplots(nrows=3, ncols=len(self._final_metrics_op.keys()), figsize=(30, 15))

		for ax, col in zip(axs[0, :], self._final_metrics_op.keys()):
			ax.set_title(col, size=14)

		for ax, row in zip(axs[:, 0], ['Silhouette', 'Calinski', 'Davies Bouldin']):
			ax.set_ylabel(row, size=14)

		for i, scores_dict in enumerate(self._final_metrics_op):
			metrics = self._final_metrics_op[scores_dict]
			for j, results in enumerate(metrics):
				min_err = np.array(metrics[results]['mean']) - np.array(metrics[results]['min'])
				max_err = np.array(metrics[results]['max']) - np.array(metrics[results]['mean'])
				err = np.array([min_err, max_err])
				axs[j][i].errorbar(x=self.neighbor_algorithims, y=metrics[results]["mean"], yerr=err, marker='o', color=_colors[results])
		plt.show()
