from utils import file_utils
from pipelines.generic_pipline import clean_numerical_data
from utils.aggregator import Aggregator
import pandas as pd

agg = Aggregator()

#
#	clustering of wines dataset
#

# 1. load file
wines_raw_data = file_utils.load_arff('datasets/wine.arff')
wines_y = pd.DataFrame(wines_raw_data['class'].apply(lambda bts: int(bts)))

# 2. clean data
wines_clean_data = clean_numerical_data(wines_raw_data, ['a' + str(num) for num in range(1, 14)])
# wines_clean_data.shape --> (178, 13)

# 3. evaluate algorithms
agg.evaluate(wines_clean_data, 2)


manually_selected_k = {
	'k_means': 3,
	'k_medoids_fast': 2,
	'fuzzy_c_means': 3
}
# 4. plot algorithms/iterations with error to choose k
agg.plot_metrics_with_error()

# 5. plot scatters
agg.plot_scatter_for_k(manually_selected_k, 3)
agg.scatter_actual_label(wines_y['class'].to_numpy())

# 6. plot external validation metrics
agg.compute_confusion_matrix(manually_selected_k, wines_y['class'].to_numpy(), 3)
agg.plot_metrics_matching_sets()

# 7. plot internal validation metrics
agg.plot_metrics_p2p_correlation()

# 8. evaluate optics
agg.evaluate_OPTICS(wines_clean_data, samp=7, x1=0.03, clust1=.05)  # if reachability plot is fairly flat then you want a low x1

# 9. plot internal metrics of optics
agg.plot_metrics_with_error_OPTICS()

# 10. pick best params
manually_selected_k = {
	'euclidean': "brute",
	'l1': "brute",
	'chebyshev': "brute"
}

# 11.plot optics related metrics
agg.compute_confusion_matrix_OPTICS(manually_selected_k, wines_y['class'].to_numpy())
agg.plot_metrics_matching_sets_OPTICS()
agg.plot_metrics_p2p_correlation_OPTICS()
