from utils import file_utils
from pipelines.generic_pipline import clean_data_and_transform
from pipelines.generic_pipline import clean_text_data
from utils.aggregator import Aggregator
import pandas as pd

agg = Aggregator()

#agg.plot_metrics_with_error('autos.arff')
#	5.2  plot scatter with best silhouette_score
#agg.plot_scatter_for_k(3, 'autos')


# 1. load file
hypo_raw_data = file_utils.load_arff('datasets/nursery.arff')
print("finished drop")
hypo_raw_data = hypo_raw_data.sample(frac=1).reset_index(drop=True)
hypo_raw_data = hypo_raw_data
hypo_y = pd.DataFrame(hypo_raw_data['class'])#.apply(lambda bts: int(bts)))

# 2. define columns type of data frame
class_column_name = 'class'
hypo_string_columns_ordinal = ['a6']
hypo_string_columns_one_hot = ['a1','a2','a3','a4','a5','a7','a8']

# 3. clean data
hypo_clean_data = clean_text_data(hypo_raw_data, hypo_string_columns_ordinal, hypo_string_columns_one_hot)

# 4. evaluate algorithms
n_iter=10
agg.evaluate(hypo_clean_data, n_iter)


agg.plot_metrics_with_error()

x=int(input("Enter k_means:"))
y=int(input("Enter k_medoids:"))
z=int(input("Enter fuzzy:"))

manually_selected_k = {
    'k_means': x,
    'k_medoids_fast': y,
    'fuzzy_c_means': z
}

# 4. plot scatter
agg.plot_scatter_for_k(manually_selected_k, n_iter)

# 5. plot Metrics matching sets
agg.compute_confusion_matrix(manually_selected_k, hypo_y[class_column_name].to_numpy(), n_iter)
agg.plot_metrics_matching_sets()
agg.plot_metrics_p2p_correlation()


# 4. evaluate algorithms
#agg.evaluate_OPTICS(hypo_clean_data, samp=5, x1=0.05, clust1=.05)


#agg.plot_metrics_with_error()

#manually_selected_k = {
#    'euclidean': "brute",
##    'l1': "brute",
#    'chebyshev': "brute"
#}

# 4. plot scatter
#agg.plot_scatter_for_k(manually_selected_k)

# 5. plot Metrics matching sets
#agg.compute_confusion_matrix_OPTICS(manually_selected_k, hypo_y[class_column_name].to_numpy())
#agg.plot_metrics_matching_sets()
#agg.plot_metrics_p2p_correlation()

