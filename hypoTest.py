from utils import file_utils
from pipelines.generic_pipline import clean_data_and_transform
from utils.aggregator import Aggregator
import pandas as pd

agg = Aggregator()



dataset_filename = 'hypothyroid.arff'

class_column_name = 'Class'
hypo_numeric_columns = ['age', 'TSH', 'T3', 'TT4', 'T4U', 'FTI']
hypo_string_columns_ordinal = ['sex', 'on_thyroxine', 'query_on_thyroxine', 'on_antithyroid_medication','sick','pregnant','thyroid_surgery','I131_treatment','query_hypothyroid','query_hyperthyroid','lithium','goitre','tumor','hypopituitary','psych']
hypo_string_columns_one_hot = ['referral_source']


# 1. load file
hypo_raw_data = file_utils.load_arff(f'datasets/{dataset_filename}')
hypo_raw_data = hypo_raw_data
data_y = pd.DataFrame(hypo_raw_data[class_column_name])
hypo_raw_data.dropna(subset = ['age', 'TSH', 'T3', 'TT4', 'T4U', 'FTI','sex'], inplace=True)
# 2. clean data
hypo_clean_data = clean_data_and_transform(hypo_raw_data, hypo_numeric_columns, hypo_string_columns_ordinal, hypo_string_columns_one_hot)


# 4. evaluate algorithms
#n_iter=2
#agg.evaluate(hypo_clean_data, n_iter)


#agg.plot_metrics_with_error()

#x=int(input("Enter k_means:"))
#y=int(input("Enter k_medoids:"))
#z=int(input("Enter fuzzy:"))

#manually_selected_k = {
#    'k_means': x,
#    'k_medoids_fast': y,
#    'fuzzy_c_means': z
#}

# 4. plot scatter
#agg.plot_scatter_for_k(manually_selected_k, n_iter)

# 5. plot Metrics matching sets
#agg.compute_confusion_matrix(manually_selected_k, data_y[class_column_name].to_numpy(), n_iter)
#agg.plot_metrics_matching_sets()
#agg.plot_metrics_p2p_correlation()


# 4. evaluate algorithms
agg.evaluate_OPTICS(hypo_clean_data, samp=8, x1=0.01, clust1=.05) #if reachability plot is fairly flat then you want a low x1


agg.plot_metrics_with_error_OPTICS()

#x=input("Enter k_means:")
#y=input("Enter k_medoids:")
#z=input("Enter fuzzy:")

manually_selected_k = {
    'euclidean': "brute",
    'l1': "brute",
    'chebyshev': "brute"
}

# 5. plot Metrics matching sets
agg.compute_confusion_matrix_OPTICS(manually_selected_k, data_y[class_column_name].to_numpy())
agg.plot_metrics_matching_sets_OPTICS()
agg.plot_metrics_p2p_correlation_OPTICS()
