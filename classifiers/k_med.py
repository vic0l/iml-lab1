import numpy as np
from utils.clustering_utils import assign_cluster, change_in_clusters, compute_labels


class KMedoid:
    """
    implementation of K-Medoid clustering algorithm
    """
    medoid = None

    def __init__(self):
        pass

    def train(self, df, n_cluster, distance_metric, seed=4):
        """
        :param distance_metric -> 	should be a function which calculate distance
                                    might be l1(for k-medoids),l2(for k-means)
        """
        n_attributes = df.shape[1]
        np.random.seed(seed)

        #v = {}
        #v_old = {}
        #for i in range(n_cluster):
        #    v[i] = df[np.random.randint(0, len(df))]
        #    v_old[i] = df[np.random.randint(0, len(df))]

        v_choices = np.random.choice(df.shape[0], n_cluster, replace=False)
        v = df[v_choices]  # (n_cluster, n_attributes)
        v_old = 0.01 * v.copy()


        n_iter = 0

        #while self.change_in_med(v, v_old) and n_iter < 100:
        while change_in_clusters(v, v_old) and n_iter < 100:
            clusters = {}
            for i in range(df.shape[0]):
                x = df[i, :]
                cluster, distance = assign_cluster(x, v, distance_metric)
                if cluster in clusters:
                    clusters[cluster].append(x)
                else:
                    clusters[cluster] = [x]

            v_old = v.copy()
            count = 0
            for clust in clusters:
                testClust = np.array(clusters[clust])
                total = []
                for i in range(len(testClust)):
                    value=0
                    for j in range(len(testClust)):
                        cluster, distance = assign_cluster(testClust[i], testClust[j], distance_metric)
                        value=value+distance
                    total.append(value)
                v[count, :] = testClust[np.argmin(total)]
                count += 1
            n_iter += 1

        # print(f'Number of iterations: {n_iter}')
        self.medoid = v
        return v


    def predict(self, df, distance_metric):
        try:
            return compute_labels(df, self.medoid, distance_metric)
        except:
            raise Exception('No clusters trained')
