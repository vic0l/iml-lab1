from utils import file_utils
from pipelines.generic_pipline import clean_data_and_transform
from pipelines.generic_pipline import clean_text_data
from utils.aggregator import Aggregator
import pandas as pd

agg = Aggregator()

#agg.plot_metrics_with_error('autos.arff')
#	5.2  plot scatter with best silhouette_score
#agg.plot_scatter_for_k(3, 'autos')


# 1. load file
hypo_raw_data = file_utils.load_arff('datasets/hypothyroid.arff')

hypo_raw_data=hypo_raw_data.dropna(subset = ['age', 'TSH', 'T3', 'TT4', 'T4U', 'FTI','sex', 'on_thyroxine', 'query_on_thyroxine', 'on_antithyroid_medication','sick','pregnant','thyroid_surgery','I131_treatment','query_hypothyroid','query_hyperthyroid','lithium','goitre','tumor','hypopituitary','psych','referral source'], inplace=True)
print("finished drop")
hypo_y = pd.DataFrame(hypo_raw_data['Class'])

# 2. define columns type of data frame
class_column_name = 'Class'
hypo_numeric_columns = ['age', 'TSH', 'T3', 'TT4', 'T4U', 'FTI']
hypo_string_columns_ordinal = ['sex', 'on_thyroxine', 'query_on_thyroxine', 'on_antithyroid_medication','sick','pregnant','thyroid_surgery','I131_treatment','query_hypothyroid','query_hyperthyroid','lithium','goitre','tumor','hypopituitary','psych']
hypo_string_columns_one_hot = ['referral source']

# 3. clean data
hypo_clean_data = clean_data_and_transform(hypo_raw_data[:5], hypo_numeric_columns, hypo_string_columns_ordinal, hypo_string_columns_one_hot)

# 4. evaluate algorithms
n_iter=10
agg.evaluate(hypo_clean_data, n_iter)


agg.plot_metrics_with_error()

x=int(input("Enter k_means:"))
y=int(input("Enter k_medoids:"))
z=int(input("Enter fuzzy:"))

manually_selected_k = {
    'k_means': x,
    'k_medoids_fast': y,
    'fuzzy_c_means': z
}

# 4. plot scatter
agg.plot_scatter_for_k(manually_selected_k, n_iter)

# 5. plot Metrics matching sets
agg.compute_confusion_matrix(manually_selected_k, hypo_y[class_column_name].to_numpy(), n_iter)
agg.plot_metrics_matching_sets()
agg.plot_metrics_p2p_correlation()