import numpy as np
from utils.clustering_utils import assign_cluster, change_in_clusters, compute_labels
import ast


class KMedoidFast:
    """
    implementation of K-Medoid clustering algorithm
    """
    medoid = None

    def __init__(self):
        pass

    def train(self, df, n_cluster, distance_metric, seed=4):
        """
        :param distance_metric -> 	should be a function which calculate distance
                                    might be l1(for k-medoids),l2(for k-means)
        """
        np.random.seed(seed)

        total = []
        for i in range(len(df)):
            value = 0
            count = 0
            for j in range(len(df)):
                value = value + distance_metric(df[i], df[j])
                count = count + 1
            total.append((value / (count - 1), i))

        total = sorted(total)
        v_choices = [pair[1] for pair in total[:n_cluster]]
        v = df[v_choices]  # (n_cluster, n_attributes)
        v_old = 0.01 * v.copy()

        n_iter = 0
        while change_in_clusters(v, v_old) and n_iter < 100:
            clusters = {}
            for i in range(df.shape[0]):
                x = df[i, :]
                cluster, distance = assign_cluster(x, v, distance_metric)
                if cluster in clusters:
                    clusters[cluster].append(x)
                else:
                    clusters[cluster] = [x]

            v_old = v.copy()
            for count, clust in enumerate(clusters):
                testClust = np.array(clusters[clust])
                total = []
                for i in range(len(testClust)):
                    value = 0
                    for j in range(len(testClust)):
                        value += distance_metric(testClust[i], testClust[j])
                    total.append(value)
                v[count, :] = testClust[np.argmin(total)]
            n_iter += 1

        # print(f'Number of iterations: {n_iter}')
        self.medoid = v
        return v

    def predict(self, df, distance_metric):
        try:
            return compute_labels(df, self.medoid, distance_metric)
        except:
            raise Exception('No clusters trained')
