import numpy as np
from utils.clustering_utils import change_in_clusters


class FuzzyCMeans:
    """
    A class used to represent the algorithm Fuzzy C Means

    ...

    Attributes
    ----------
    U : numpy.ndarray
        Its shape is (n_cluster, n). It will depend on the parameter
        n_cluster, which is defined in the method train and it
        is independent of the class.
        It is the membership matrix from the FCM algorithm. Every column
        will determine the membership of an instance to the clusters.
    V : numpy.ndarray
        Its shape is (n_cluster, n_features). It will contain the centroids
        of FCM. Each centroid will be a (1, n_feature) array.
    m : float
        Hyper-parameter that controls how fuzzy the clusters will be.

    Methods
    -------
    compute_performance_index(df):
        It computes the performance index of the FCM, once it has been
        trained, given a dataframe df.
    train(df, n_cluster, distance_metric, seed=4):
        Given a dataframe, the number of clusters, a distance metric and
        a random seed it will iterate until the parameters U and V have been
        trained.
    predict():
        It returns the crisp clustering
    """

    U, V = None, None

    def __init__(self, m=2):
        self.m = m

    def compute_performance_index(self, df):
        '''
		It computes the performance index of a given fuzzy clustering.

		    Parameters:
		        df (numpy.ndarray): Numpy array of shape (n, n_features) that
		            contains the data where the performance index will be
		            calculated.

		    Returns:
		        performance_index (float): Float that contains the performance
		        index of the clustering
		'''

        n = df.shape[0]
        n_cluster = self.V.shape[0]
        average_samples = df.sum(axis=0) / n

        performance_index = 0
        for i in range(n_cluster):
            positives = ((df - self.V[i, :]) ** 2).sum(axis=1)
            negative = ((self.V[i, :] - average_samples) ** 2).sum()

            u_i_pow_m = (self.U[i, :]) ** self.m

            performance_index += u_i_pow_m.dot(positives - negative)

        return performance_index

    def train(self, df, n_cluster, distance_metric, seed=4):
        '''
        Given a dataframe df, number of clusters n_cluster, a distance
        metric distance_metric and a random seed seed, it will iterate to determine
        the parameters U and V, native to the FuzzyCMeans class. It follows the
        FCM algorithm, updating U first and then V.

        The condition to end the loop is that the change in V is lower than an
        epsilon or that the iterations surpass 100.

        When the loop ends, it will return

            Parameters:
                df (numpy.ndarray): Numpy array of shape (n, n_features) that contains
                    the data to cluster.
                n_cluster (int): Integer that determines the number of clusters that
                    the algorithm will be trained for.
                distance_metric (function): function that computes the distance between
                    two 1D arrays of the same shape.
                seed (int): integer that sets the random seed for the initialization.

            Returns:

        '''

        n, n_features = df.shape[0], df.shape[1]
        np.random.seed(seed)

        # INITIALIZATION
        U = np.random.rand(n_cluster, n)
        U = U / U.sum(axis=1)[:, None]

        # V
        v_choices = np.random.choice(n, n_cluster, replace=False)
        V = df[v_choices]
        V = V + np.random.randn(V.shape[0], V.shape[1])
        V_old = 0.5 * df[v_choices]

        n_iter = 0
        while change_in_clusters(V, V_old) and n_iter < 100:
            V_old = V.copy()

            # Update U
            for k in range(n):
                for i in range(n_cluster):
                    den = np.zeros(n_cluster)
                    for j in range(n_cluster):
                        den[j] = distance_metric(df[k, :], V[j, :])

                    inside_power = distance_metric(df[k, :], V[i, :]) / den
                    U[i, k] = 1 / (np.power(inside_power,
                                            2 / (self.m - 1))).sum()

            # Update V
            U_pow_m = np.power(U, self.m)
            for i in range(n_cluster):
                V[i, :] = (U_pow_m[i, :][:, None] * df).sum(axis=0)
            V = V / U_pow_m.sum(axis=1)[:, None]

            n_iter += 1

        # print(f'Number of iterations: {n_iter}')
        self.V = V
        self.U = U

    def predict(self):
        '''
        It creates the crisp clustering based on the membership matrix U of
        Fuzzy C Means algorithm. For every instance, the input is an array of
        membership values to every cluster (K in total). It selects the maximum
        and returns the label of that cluster.

            Parameters:

            Returns:
                label (list): List that contains the labels of the trained algorithm
        '''

        try:
            labels = self.U.argmax(axis=0)
            return labels
        except:
            raise Exception('No clusters trained')
