import numpy as np
from sklearn.cluster import OPTICS
from matplotlib import pyplot as plt

class Optics:
	"""
		Abstract class which define the API for implementations
	"""
	distance_m = "euclidean"
	neighbor= 'brute'

	def __init__(self, distance_metric=2, neighbor_alg='brute'):
		"""
			@:param number_of_cluster -> sets the metric, 2= euclidean, 3=l1, 4=jaccard
			@:param distance_measure -> algorithm being used, it will be either brute or ball Tree
		"""
		if distance_metric == 2:
			self.distance_m = "euclidean"
		if distance_metric == 3:
			self.distance_m = "l1"
		if distance_metric == 4:
			self.distance_m = "chebyshev"
		self.neighbor = neighbor_alg

	def train(self, data,pcaVal, samples=5,x=0.05,clust=.05,):
		"""
			Should train the classifier and return a map(dict) with results of classification
			@:param data -> self explanatory data on which we want to perform clustering
		"""
		self.optics_model = OPTICS(min_samples=samples, xi=x, min_cluster_size=clust, metric=self.distance_m, algorithm=self.neighbor)
		self.optics_model.fit(data)
		self.space = np.arange(len(data))
		self.reachability = self.optics_model.reachability_[self.optics_model.ordering_]
		self.preLabels=self.optics_model.labels_[self.optics_model.ordering_]


		#raise NotImplementedError("Implement method in subclass")

		x = np.random.randint(100, 300)
		plt.figure(figsize=(10, 7))
		colors = ["g.", "r.", "b.", "y.", "c."]

		plt.subplot(211)
		for klass, color in zip(range(0, 5), colors):
			Xk = self.space[self.preLabels == klass]
			Rk = self.reachability[self.preLabels == klass]
			plt.plot(Xk, Rk, color, alpha=0.3)

		plt.plot(self.space[self.preLabels == -1], self.reachability[self.preLabels == -1], "k.", alpha=0.3)
		plt.plot(self.space, np.full_like(self.space, 2.0, dtype=float), "k-", alpha=0.5)
		plt.plot(self.space, np.full_like(self.space, 0.5, dtype=float), "k-.", alpha=0.5)
		plt.ylabel("Reachability" + self.distance_m)
		plt.title("Reachability Plot for "+ self.distance_m + " and " + self.neighbor)

		plt.subplot(212)
		colors = ["g.", "r.", "b.", "y.", "c."]
		for klass, color in zip(range(0, 5), colors):
			lbl_kk = pcaVal[self.optics_model.labels_ == klass]
			plt.plot(lbl_kk[:, 0], lbl_kk[:, 1], color, alpha=0.3)

		plt.plot(data[self.optics_model.labels_ == -1, 0], data[self.optics_model.labels_ == -1, 1], "k+", alpha=0.1)
		plt.title("Automatic Clustering - OPTICS")
		plt.savefig("./figures/figure"+str(x)+".png")
		plt.close(x)

		return self.preLabels
