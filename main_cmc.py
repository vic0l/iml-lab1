from utils import file_utils
from pipelines.generic_pipline import clean_data_and_transform
from utils.aggregator import Aggregator
import pandas as pd

agg = Aggregator()

#
#	clustering of cmc dataset
#

dataset_filename = 'cmc.arff'

class_column_name = 'class'
numeric_columns = ['wage', 'children']
columns_ordinal = ['weducation', 'heducation', 'wreligion', 'wworking', 'living_index', 'media_exposure']
columns_one_hot = ['hoccupation']

# 1. load file
raw_data = file_utils.load_arff(f'datasets/{dataset_filename}')
data_y = pd.DataFrame(raw_data[class_column_name].apply(lambda bts: int(bts)))

# 2. clean data
clean_data = clean_data_and_transform(raw_data, numeric_columns, columns_ordinal, columns_one_hot)

# 3. evaluate algorithms
n_iter = 5
agg.evaluate(clean_data, n_iter)

manually_selected_k = {
    'k_means': 3,
    'k_medoids_fast': 3,
    'fuzzy_c_means': 3
}

# 4. plot scatter
agg.plot_scatter_for_k(manually_selected_k, n_iter)

# 5. plot algorithms/iterations with error to choose k
agg.plot_metrics_with_error()

# 6. plot external validation metrics
agg.compute_confusion_matrix(manually_selected_k, data_y[class_column_name].to_numpy(), n_iter)
agg.plot_metrics_matching_sets()

# 7. plot internal validation metrics
agg.plot_metrics_p2p_correlation()

# 8. evaluate OPTICS
agg.evaluate_OPTICS(clean_data, samp=5, x1=0.001, clust1=.05)
agg.plot_metrics_with_error_OPTICS()

manually_selected_distances = {
    'euclidean': "kd_tree",
    'l1': "kd_tree",
    'chebyshev': "kd_tree"
}

# 9. plot Metrics matching sets
agg.compute_confusion_matrix_OPTICS(manually_selected_distances, data_y[class_column_name].to_numpy())
agg.plot_metrics_matching_sets_OPTICS()
agg.plot_metrics_p2p_correlation_OPTICS()
