
from utils import file_utils
from pipelines.generic_pipline import clean_data_and_transform
from sklearn.datasets import make_blobs
from sklearn.cluster import OPTICS
import numpy as np
import matplotlib.pyplot as plt


PATH_TO_FILE = 'datasets/autos.arff'

# load the data as pd.DataFrame from arff file
raw_data = file_utils.load_arff(PATH_TO_FILE)

numeric_columns = ['wheel-base', 'price', 'horsepower', 'peak-rpm', 'wheel-base', 'length', 'bore', 'stroke',
				   'city-mpg', 'highway-mpg']
string_columns_ordinal = ['num-of-doors', 'fuel-type', 'drive-wheels', 'engine-location', ]

string_columns_one_hot= ['fuel-system']

clean_data = clean_data_and_transform(raw_data, numeric_columns, string_columns_ordinal,string_columns_one_hot)

print(clean_data.shape)
print (type(clean_data))
##

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import OPTICS

metricType=["euclidean","l1","braycurtis", "canberra", "chebyshev","jaccard"]

count=0
for i in metricType:
	print (i)
	# Configuration options
	optics_model = OPTICS(min_samples = 10, xi = 0.05, min_cluster_size = 0.05,metric=i,algorithm="brute")
	optics_model2 = OPTICS(min_samples=10, xi=0.05, min_cluster_size=0.05, metric=i, algorithm="ball_tree")
	optics_model.fit(clean_data)
	optics_model2.fit(clean_data)

	# Creating a numpy array with numbers at equal spaces till
	# the specified range
	space = np.arange(len(clean_data))

	# Storing the reachability distance of each point
	reachability = optics_model.reachability_[optics_model.ordering_]
	reachability2 = optics_model2.reachability_[optics_model.ordering_]


	# Storing the cluster labels of each point
	labels = optics_model.labels_[optics_model.ordering_]
	labels2 = optics_model2.labels_[optics_model.ordering_]
	print (set(labels))
	colors = ['c.', 'b.', 'r.', 'y.', 'g.']
	for Class, colour in zip(range(0, 5), colors):
		Xk = clean_data[optics_model2.labels_ == Class]
		plt.plot(Xk[:, 0], Xk[:, 1], colour, alpha=0.3)
	# Plotting the OPTICS Clustering

	plt.figure(count)
	plt.plot(clean_data[optics_model2.labels_ == -1, 0],
			 clean_data[optics_model2.labels_ == -1, 1],
			 'k+', alpha=0.1)
	plt.title('OPTICS Clustering for '+ i)

	count=count+1

plt.tight_layout()
plt.show()
