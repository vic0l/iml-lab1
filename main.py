from utils import file_utils
from pipelines.generic_pipline import clean_data_and_transform
from pipelines.generic_pipline import clean_numerical_data
from utils.aggregator import Aggregator
import pandas as pd

agg = Aggregator()

#
#  	clustering of autos dataset
#


# 1. load file
#autos_raw_data = file_utils.load_arff('datasets/autos.arff')

# 2. define columns type of data frame
#autos_numeric_columns = ['wheel-base', 'price', 'horsepower', 'peak-rpm', 'wheel-base', 'length', 'bore', 'stroke', 'city-mpg', 'highway-mpg']
#autos_string_columns_ordinal = ['num-of-doors', 'fuel-type', 'drive-wheels', 'engine-location']
#autos_string_columns_one_hot = ['fuel-system']

# 3. clean data
#autos_clean_data = clean_data_and_transform(autos_raw_data, autos_numeric_columns, autos_string_columns_ordinal, autos_string_columns_one_hot)

# 4. run aggregated training
#agg.evaluate(autos_clean_data)
# 5. plot results
# 	5.1  because for autos.arr we don't have classes
# 		 we measure the score using plot_silhouette_score
#agg.plot_metrics_with_error('autos.arff')
#	5.2  plot scatter with best silhouette_score
#agg.plot_scatter_for_k(3, 'autos')

agg = Aggregator()

#
#	clustering of wines dataset
#

# 1. load file
wines_raw_data = file_utils.load_arff('datasets/wine.arff')
wines_y = pd.DataFrame(wines_raw_data['class'].apply(lambda bts: int(bts)))

# 2. clean data
wines_clean_data = clean_numerical_data(wines_raw_data, ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10', 'a12', 'a13'])

# 3. evaluate algorithms
agg.evaluate(wines_clean_data)

# 4. plot scatter
agg.plot_scatter_for_k(3, 'wines.arff')


